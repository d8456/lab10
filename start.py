import os
import sys
print('\n'.join(sys.path) + '\n')
import logging
import pkg1.requesting
import pkg1.extraction
import pkg2.configreading as configreading

def validArgs(args):
    return len(args) == 2 and os.path.isfile(args[1])

def main(file: str):
    menu = "1. Search by name\n2. Search by Id\n0. Exit\n"
    
    url, apikey = configreading.readConfig(file)
    getter = pkg1.requesting.MovieGetter(url, apikey)
    print(menu)
    while((option := input()) != '0'):
        if option == '1':
            movieName = input("Name: ")
            dataJsonStr = getter.getMoviesByName(movieName)
            print(pkg1.extraction.getPositions(3, dataJsonStr))
        if option == '2':
            movieId = input("Id: ")
            dataJsonStr = getter.getMovieById(movieId)
            print(pkg1.extraction.getPrettyDetails(dataJsonStr))
        print('\n' + menu)

if __name__ == "__main__":
    if validArgs(sys.argv):
        logging.basicConfig(filename='example.log',
            encoding='utf-8', level=logging.DEBUG)
        main(sys.argv[1])